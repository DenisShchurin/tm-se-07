package ru.shchurin.tm.service;

import ru.shchurin.tm.api.TaskService;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.api.TaskRepository;
import ru.shchurin.tm.exception.*;
import java.util.List;

public final class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        return taskRepository.findAll(userId);
    }

    @Override
    public Task findOne(final String userId, final String id) throws ConsoleIdException, TaskNotFoundException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        final Task task = taskRepository.findOne(userId, id);
        if (task == null)
            throw new TaskNotFoundException();
        return task;
    }

    @Override
    public void persist(final Task task) throws AlreadyExistsException, ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (task == null)
            return;
        if (task.getName() == null || task.getName().isEmpty())
            throw new ConsoleNameException();
        if (task.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (task.getEndDate() == null)
            throw new ConsoleEndDateException();
        taskRepository.persist(task);
    }

    @Override
    public void merge(final Task task) throws ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (task == null)
            return;
        if (task.getName() == null || task.getName().isEmpty())
            throw new ConsoleNameException();
        if (task.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (task.getEndDate() == null)
            throw new ConsoleEndDateException();
        taskRepository.merge(task);
    }

    @Override
    public void remove(final String userId, final String id) throws ConsoleIdException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        taskRepository.remove(userId, id);
    }

    @Override
    public void removeAll (final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        taskRepository.removeAll(userId);
    }

    @Override
    public void removeByName(final String userId, final String name) throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        taskRepository.removeByName(userId, name);
    }
}
