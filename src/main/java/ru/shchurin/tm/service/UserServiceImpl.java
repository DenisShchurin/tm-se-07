package ru.shchurin.tm.service;

import ru.shchurin.tm.api.UserService;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.api.UserRepository;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public final class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findOne(final String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        return userRepository.findOne(id);
    }

    @Override
    public void persist(final User user) throws AlreadyExistsException, ConsoleLoginException, ConsoleHashPasswordException {
        if (user == null) {
            return;
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new ConsoleLoginException();
        }
        if (user.getHashPassword() == null || user.getHashPassword().isEmpty()) {
            throw new ConsoleHashPasswordException();
        }
        userRepository.persist(user);
    }

    @Override
    public void merge(final User user) throws ConsoleLoginException {
        if (user == null) {
            return;
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new ConsoleLoginException();
        }
        userRepository.merge(user);
    }

    @Override
    public void remove(final String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        userRepository.remove(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public void removeByLogin(final String login) throws ConsoleLoginException {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        userRepository.removeByLogin(login);
    }

    @Override
    public String getHashOfPassword(final String password) throws NoSuchAlgorithmException, ConsolePasswordException {
        if (password == null || password.isEmpty()) {
            throw new ConsolePasswordException();
        }
        final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        final byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

    @Override
    public User authoriseUser(final String login, final String hashOfPassword) throws ConsoleLoginException, ConsoleHashPasswordException {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        if (hashOfPassword == null || hashOfPassword.isEmpty()) {
            throw new ConsoleHashPasswordException();
        }
        final Collection<User> users = userRepository.findAll();
        for (User user : users) {
            if (user.getLogin().equals(login) && user.getHashPassword().equals(hashOfPassword)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public boolean updatePassword(final String login, final String hashPassword, final String newHashPassword) throws UserNotAuthorized, ConsolePasswordException {
        if (login == null || login.isEmpty())
            throw new UserNotAuthorized();
        if (hashPassword == null || hashPassword.isEmpty())
            throw new UserNotAuthorized();
        if (newHashPassword == null || newHashPassword.isEmpty())
            throw new ConsolePasswordException();
        return userRepository.updatePassword(login, hashPassword, newHashPassword);
    }
}
