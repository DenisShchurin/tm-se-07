package ru.shchurin.tm.service;

import ru.shchurin.tm.api.ProjectService;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.api.ProjectRepository;
import ru.shchurin.tm.exception.*;
import java.util.List;

public final class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        return projectRepository.findAll(userId);
    }

    @Override
    public Project findOne(final String userId, final String id) throws ConsoleIdException, ProjectNotFoundException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        final Project project = projectRepository.findOne(userId, id);
        if (project == null)
            throw  new ProjectNotFoundException();
        return project;
    }

    @Override
    public void persist(final Project project) throws AlreadyExistsException, ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (project == null)
            return;
        if (project.getName() == null || project.getName().isEmpty())
            throw new ConsoleNameException();
        if (project.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (project.getEndDate() == null)
            throw new ConsoleEndDateException();
        projectRepository.persist(project);
    }

    @Override
    public void merge(final Project project) throws ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (project == null)
            return;
        if (project.getName() == null || project.getName().isEmpty())
            throw new ConsoleNameException();
        if (project.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (project.getEndDate() == null)
            throw new ConsoleEndDateException();
        projectRepository.merge(project);
    }

    @Override
    public void remove(final String userId, final String id) throws ConsoleIdException, UserNotAuthorized {
        if (userId == null || userId.isEmpty()) throw new UserNotAuthorized();
        if (id == null || id.isEmpty()) throw new ConsoleIdException();
        projectRepository.remove(userId, id);
    }

    @Override
    public void removeAll(final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        projectRepository.removeAll(userId);
    }

    @Override
    public void removeByName(final String userId, final String name) throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        projectRepository.removeByName(userId, name);
    }
}
