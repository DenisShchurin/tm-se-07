package ru.shchurin.tm.exception;

public class UserNotAuthorized extends Exception {
    public UserNotAuthorized() {
        super("You have not authorized");
    }
}
