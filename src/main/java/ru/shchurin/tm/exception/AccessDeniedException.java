package ru.shchurin.tm.exception;

public class AccessDeniedException extends Exception {
    public AccessDeniedException() {
        super("Access Denied");
    }
}
