package ru.shchurin.tm.api;

import ru.shchurin.tm.entity.Task;

import java.util.List;

public interface ProjectTaskService {
    List<Task> getTasksOfProject(String userId, String name) throws Exception;

    void removeProjectAndTasksByName(String userId, String name) throws Exception;
}
