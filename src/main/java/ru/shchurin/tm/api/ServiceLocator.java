package ru.shchurin.tm.api;

import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.User;

import java.util.List;

public interface ServiceLocator {
    List<AbstractCommand> getCommands();

    ProjectService getProjectService();

    TaskService getTaskService();

    ProjectTaskService getProjectTaskService();

    UserService getUserService();

    User getCurrentUser();

    void setCurrentUser(final User currentUser);
}
