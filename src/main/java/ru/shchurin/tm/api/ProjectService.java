package ru.shchurin.tm.api;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.exception.*;

import java.util.List;

public interface ProjectService {
    List<Project> findAll(String userId) throws Exception;

    Project findOne(String userId, String id) throws Exception;

    void persist(Project project) throws Exception;

    void merge(Project project) throws Exception;

    void remove(String userId, String id) throws Exception;

    void removeAll(String userId) throws Exception;

    void removeByName(String userId, String name) throws Exception;
}
