package ru.shchurin.tm.api;


import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.exception.AlreadyExistsException;

import java.util.List;

public interface TaskRepository {
    List<Task> findAll(String userId);

    Task findOne(String userId, String id);

    void persist(Task task) throws AlreadyExistsException;

    void merge(Task task);

    void remove(String userId, String id);

    void removeAll(String userId);

    void removeByName(String userId, String name);
}
