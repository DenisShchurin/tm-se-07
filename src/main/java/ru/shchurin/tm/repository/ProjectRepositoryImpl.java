package ru.shchurin.tm.repository;

import ru.shchurin.tm.api.ProjectRepository;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.exception.*;

import java.util.*;

public final class ProjectRepositoryImpl implements ProjectRepository {
    private final Map<String, Project> projects = new HashMap<>();

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> userProjects = new ArrayList<>();
        for (Project project : projects.values()) {
            if (userId.equals(project.getUserId()))
                userProjects.add(project);
        }
        return userProjects;
    }

    @Override
    public Project findOne(final String userId, final String id) {
        final Project project = projects.get(id);
        if (project.getUserId().equals(userId)) {
            return project;
        } else {
            return null;
        }
    }

    @Override
    public void persist(final Project project) throws AlreadyExistsException {
        if (projects.containsKey(project.getId()))
            throw new AlreadyExistsException();
        projects.put(project.getId(), project);
    }

    @Override
    public void merge(final Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void remove(final String userId, final String id) {
        final Project project = projects.get(id);
        if (project.getUserId().equals(userId))
            projects.remove(id);
    }

    @Override
    public void removeAll(final String userId) {
        for (Iterator<Map.Entry<String, Project>> it = projects.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> e = it.next();
            if (userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }

    @Override
    public void removeByName(final String userId, final String name) {
        for (Iterator<Map.Entry<String, Project>> it = projects.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> e = it.next();
            if (name.equals(e.getValue().getName()) && userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }
}
