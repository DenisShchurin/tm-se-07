package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER TASK NAME:");
        final String name = ConsoleUtil.getStringFromConsole();
        try {
            serviceLocator.getTaskService().removeByName(serviceLocator.getCurrentUser().getId(), name);
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("[TASK REMOVED]");
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
