package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.List;

public final class UserEndSessionCommand extends AbstractCommand {
    private final boolean safe = true;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "user-end-session";
    }

    @Override
    public String getDescription() {
        return "End user session";
    }

    @Override
    public void execute() {
        System.out.println("[USER END SESSION]");
        serviceLocator.setCurrentUser(null);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
