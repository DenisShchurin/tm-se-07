package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {
    private final boolean safe = true;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommands()) {
            System.out.println(command.getCommand() + ": " + command.getDescription());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
