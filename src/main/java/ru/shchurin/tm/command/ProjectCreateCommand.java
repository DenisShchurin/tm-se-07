package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;
import ru.shchurin.tm.exception.*;

import java.text.ParseException;
import java.util.*;

public final class ProjectCreateCommand extends AbstractCommand{
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = ConsoleUtil.getStringFromConsole();
        System.out.println("ENTER START_DATE:");
        final String start = ConsoleUtil.getStringFromConsole();
        final Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG START_DATE:");
            return;
        }
        System.out.println("ENTER END_DATE:");
        final String end = ConsoleUtil.getStringFromConsole();
        final Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println("YOU ENTERED WRONG END_DATE:");
            return;
        }
        try {
            serviceLocator.getProjectService().persist(new Project(name, startDate, endDate, serviceLocator.getCurrentUser().getId()));
            System.out.println("[PROJECT CREATED]");
        } catch (AlreadyExistsException | ConsoleNameException | ConsoleStartDateException | ConsoleEndDateException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}

