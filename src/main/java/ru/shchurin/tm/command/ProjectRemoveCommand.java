package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project and his tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER PROJECT NAME:");
        final String name = ConsoleUtil.getStringFromConsole();
        try {
            serviceLocator.getProjectTaskService().removeProjectAndTasksByName(serviceLocator.getCurrentUser().getId(), name);
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("[PROJECT AND HIS TASKS REMOVED]");
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
