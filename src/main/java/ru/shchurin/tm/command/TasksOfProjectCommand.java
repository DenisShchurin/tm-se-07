package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TasksOfProjectCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "tasks-of-project";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS OF PROJECT]");
        System.out.println("ENTER PROJECT NAME");
        final String name = ConsoleUtil.getStringFromConsole();
        final List<Task> tasks;
        try {
            tasks = serviceLocator.getProjectTaskService().getTasksOfProject(serviceLocator.getCurrentUser().getId(), name);
            int index = 1;
            for (Task task : tasks) {
                System.out.println(index++ + ". " + task);
            }
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
