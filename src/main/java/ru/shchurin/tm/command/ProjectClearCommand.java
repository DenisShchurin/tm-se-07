package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectClearCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getProjectService().removeAll(serviceLocator.getCurrentUser().getId());
        System.out.println("[ALL PROJECT REMOVED]");
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
