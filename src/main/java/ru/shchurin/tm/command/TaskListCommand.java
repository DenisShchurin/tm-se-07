package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @Override
    public String getCommand() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[PROJECT LIST]");
        int index = 1;
        for (Task task : serviceLocator.getTaskService().findAll(currentUser.getId())) {
            if (task.getUserId().equals(currentUser.getId())) {
                System.out.println(index++ + ". " + task);
            }
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
