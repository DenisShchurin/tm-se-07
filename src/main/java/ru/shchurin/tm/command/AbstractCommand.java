package ru.shchurin.tm.command;

import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Role;

import java.util.List;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getCommand();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean isSafe();

    public abstract List<Role> getRoles();
}
