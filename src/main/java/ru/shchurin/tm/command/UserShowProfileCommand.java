package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserShowProfileCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "user-show-profile";
    }

    @Override
    public String getDescription() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[SHOW USER PROFILE]");
        System.out.println(currentUser.getLogin() + ", " + currentUser.getRole());
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
