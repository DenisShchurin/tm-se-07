package ru.shchurin.tm.entity;

import ru.shchurin.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

public final class Task {

    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;
    private Date startDate;
    private Date endDate;
    private String projectId;
    private String userId;

    public Task(String name, String projectId, Date startDate, Date endDate) {
        this.name = name;
        this.projectId = projectId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Task(String id, String name, String projectId, Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.projectId = id;
    }

    public Task(String id, String name, String projectId, Date startDate, Date endDate, String userId) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.projectId = projectId;
        this.userId = userId;
    }

    public Task(String name, String projectId, Date startDate, Date endDate, String userId) {
        this.name = name;
        this.projectId = projectId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
    }

    public Task() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TASK{" +
                "ID:'" + id + '\'' +
                ", NAME:'" + name + '\'' +
//                ", description='" + description + '\'' +
                ", START_DATE:" + DateUtil.dateFormat(startDate) +
                ", END_DATE:" + DateUtil.dateFormat(endDate) +
                ", PROJECT_ID:'" + projectId + '\'' +
                '}';
    }
}
