## Taskmanager
Simple CRUD application


## Prerequisites
Windows 10
JRE 8


## Technologies
MAVEN, GITLAB, GIT-IGNORE, MANIFEST.MF, MAVEN-JAR-PLUGIN


## Authors
Denis Shchurin
denisshchurin@gmail.com


## Command for building project

```
mvn compile
```


## Command for execution project

```
java -jar target/taskmanager-1.0-SNAPSHOT.jar
```





